package com.example.scorp.repository;

import com.example.scorp.entity.Follow;
import com.example.scorp.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.HashSet;
import java.util.List;

public interface FollowRepository extends JpaRepository<Follow, Long> {

    @Query(value = "SELECT follower_id FROM follow WHERE follower_id = :follower AND following_id IN (:followings) ", nativeQuery = true)
    HashSet<Long> getUsersByUserIdList(@Param("follower") Long follower, @Param("followings") List<Long> followings);

}
