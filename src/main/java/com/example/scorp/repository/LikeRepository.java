package com.example.scorp.repository;

import com.example.scorp.entity.Likes;
import com.example.scorp.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;

@Repository
public interface LikeRepository extends JpaRepository<Likes, Long> {

    @Query(value = "SELECT post_id FROM likes WHERE post_id IN (:posts) AND user_id = :userId", nativeQuery = true)
    HashSet<Long> getLikesByPostListAndUser(@Param("posts") List<Long> posts, Long userId);

}
