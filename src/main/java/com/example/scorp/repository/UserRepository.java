package com.example.scorp.repository;

import com.example.scorp.entity.Likes;
import com.example.scorp.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<Users, Long> {

    @Query(value = "SELECT * FROM users WHERE id IN (:users)", nativeQuery = true)
    List<Users> getUsersByUserIdList(@Param("users") List<Long> users);

}
