package com.example.scorp.repository;

import com.example.scorp.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PostRepository  extends JpaRepository<Post, Long> {

    @Query(value = "SELECT * FROM post WHERE id IN (:posts)", nativeQuery = true)
    List<Post> getPostsByPostList(@Param("posts") List<Long> posts);

}
