package com.example.scorp.util;

import com.example.scorp.dto.HeapNode;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;


@Data
public class MaxHeap {
    List<HeapNode> heapNodes;
    int heapSize;

    public MaxHeap(LinkedList<HeapNode> heapNodes, int size)
    {
        this.heapSize = size;
        this.heapNodes = heapNodes;
        int i = (heapSize - 1)/2;
        while(i>=0){
            MaxHeapify(i);
            i--;
        }
    }

    public void replaceMax(HeapNode root) {
        heapNodes.set(0,root);
        MaxHeapify(0);
    }

    private void MaxHeapify(int i)
    {
        int l = left(i);
        int r = right(i);
        int max = i;

        if (l < heapSize && heapNodes.get(l).getPost().getCreatedAt().getTime() >= heapNodes.get(max).getPost().getCreatedAt().getTime()){
            if(heapNodes.get(l).getPost().getCreatedAt().getTime() == heapNodes.get(max).getPost().getCreatedAt().getTime()
                    && heapNodes.get(l).getPost().getId() > heapNodes.get(max).getPost().getId()){
                max=l;
            } else if(heapNodes.get(l).getPost().getCreatedAt().getTime() > heapNodes.get(max).getPost().getCreatedAt().getTime()){
                max = l;
            }
        }
        if (r < heapSize && heapNodes.get(r).getPost().getCreatedAt().getTime() >= heapNodes.get(max).getPost().getCreatedAt().getTime()){
            if(heapNodes.get(r).getPost().getCreatedAt().getTime() == heapNodes.get(max).getPost().getCreatedAt().getTime()
                    && heapNodes.get(r).getPost().getId() > heapNodes.get(max).getPost().getId()){
                max = r;
            } else if(heapNodes.get(r).getPost().getCreatedAt().getTime() > heapNodes.get(max).getPost().getCreatedAt().getTime()) {
                max = r;
            }
        }
        if (max != i)
        {
            swap(i, max);
            MaxHeapify(max);
        }
    }

    private void swap(int i, int j) {
        HeapNode temp = heapNodes.get(i);
        heapNodes.set(i,heapNodes.get(j));
        heapNodes.set(j,temp);
    }

    int left(int i) { return (2*i + 1); }

    int right(int i) { return (2*i + 2); }
}
