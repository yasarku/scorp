package com.example.scorp.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "follow")
public class Follow extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;
    @ManyToOne
    @JoinColumn(name="follower_id", nullable=false)
    private Users follower;
    @ManyToOne
    @JoinColumn(name="following_id", nullable=false)
    private Users following;
}
