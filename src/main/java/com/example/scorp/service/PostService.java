package com.example.scorp.service;

import com.example.scorp.dto.MergePostsRequest;
import com.example.scorp.dto.PostRequestDto;
import com.example.scorp.entity.Post;

import java.util.List;
import java.util.Map;

public interface PostService {
    Map<Long, Post> findPostsByIdAndUser(PostRequestDto postRequestDto);
    List<Post> mergePostListsV2(MergePostsRequest mergePostsRequest);
}
