package com.example.scorp.service.Impl;

import com.example.scorp.dto.HeapNode;
import com.example.scorp.dto.MergePostsRequest;
import com.example.scorp.dto.PostRequestDto;
import com.example.scorp.entity.Post;
import com.example.scorp.entity.Users;
import com.example.scorp.repository.FollowRepository;
import com.example.scorp.repository.LikeRepository;
import com.example.scorp.repository.PostRepository;
import com.example.scorp.repository.UserRepository;
import com.example.scorp.service.PostService;
import com.example.scorp.util.MaxHeap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    PostRepository postRepository;

    @Autowired
    LikeRepository likeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    FollowRepository followRepository;

    @Override
    public Map<Long, Post> findPostsByIdAndUser(PostRequestDto postRequestDto) {
        List<Post> posts = postRepository.getPostsByPostList(postRequestDto.getPosts());
        Set<Long> likes = likeRepository.getLikesByPostListAndUser(postRequestDto.getPosts(), postRequestDto.getUserId());
        List<Long> userIds = new ArrayList<>();
        Map<Long, Users> userMap = new HashMap();
        Map<Long, Post> resultMap = new LinkedHashMap<>();

        for(Long postId : postRequestDto.getPosts()){
            resultMap.put(postId, null);
        }
        for(Post post : posts) {
            userIds.add(post.getUserId());
            post.setLiked(likes.contains(post.getId()));
        }
        List<Users> usersList = userRepository.getUsersByUserIdList(userIds);
        Set<Long> followed = followRepository.getUsersByUserIdList(postRequestDto.getUserId(), userIds);
        for(Users users : usersList){
            users.setFollowed(followed.contains(users.getId()));
            userMap.put(users.getId(), users);
        }

        for(Post post : posts) {
            post.setUsers(userMap.get(post.getUserId()));
            resultMap.put(post.getId(), post);
        }

        return resultMap;
    }


    @Override
    public List<Post> mergePostListsV2(MergePostsRequest mergePostsRequest) {
        LinkedList<HeapNode> nodeList = new LinkedList<>();
        HashMap<Long, Post> resultMap = new LinkedHashMap<>();
        int totalSize = 0;
        List<List<Post>> postList = mergePostsRequest.getPostList();
        for(int i = 0; i < postList.size(); i++){
            HeapNode heapNode = new HeapNode(postList.get(i).get(postList.get(i).size() -1), i, postList.get(i).size() -2);
            nodeList.add(heapNode);
            totalSize += postList.get(i).size();
        }
        MaxHeap maxHeap = new MaxHeap(nodeList, postList.size());
        LinkedList<Post> result = new LinkedList<>();

        for(int i = 0; i < totalSize; i++)
        {
            HeapNode root = maxHeap.getHeapNodes().get(0);
            resultMap.put(root.getPost().getId(), root.getPost());
            if(root.getJ() >= 0){
                root.setPost(postList.get(root.getI()).get(root.getJ()));
                root.setJ(root.getJ() - 1);
            }
            else{
                Post post = new Post();
                post.setCreatedAt(new Date(0));
                post.setId(-100L);
                root = new HeapNode(post,0,-3);
            }
            maxHeap.replaceMax(root);
        }
        result = new LinkedList<>(resultMap.values());
        return result;
    }
}
