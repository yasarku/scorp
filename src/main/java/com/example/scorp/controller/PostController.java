package com.example.scorp.controller;

import com.example.scorp.dto.MergePostsRequest;
import com.example.scorp.dto.PostRequestDto;
import com.example.scorp.dto.Response;
import com.example.scorp.dto.Result;
import com.example.scorp.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("post/")
public class PostController {

    @Autowired
    PostService postService;

    @PostMapping(path = "getPosts")
    public Response getPosts(@RequestBody PostRequestDto postRequestDto){
        return new Response(OK, new Result(postService.findPostsByIdAndUser(postRequestDto)));
    }

    @PostMapping(path = "mergePosts")
    public Response getPosts(@RequestBody MergePostsRequest mergePostsRequest){
        return new Response(OK, new Result(postService.mergePostListsV2(mergePostsRequest)));
    }
}
