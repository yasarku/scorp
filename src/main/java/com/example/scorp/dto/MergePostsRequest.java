package com.example.scorp.dto;

import com.example.scorp.entity.Post;
import lombok.Data;

import java.util.List;

@Data
public class MergePostsRequest {
    List<List<Post>> postList;
}
