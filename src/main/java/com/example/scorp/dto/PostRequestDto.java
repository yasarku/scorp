package com.example.scorp.dto;

import lombok.Data;

import java.util.List;

@Data
public class PostRequestDto {
    Long userId;
    List<Long> posts;
}
