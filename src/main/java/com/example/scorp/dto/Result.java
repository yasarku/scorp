package com.example.scorp.dto;

import lombok.Data;

@Data
public class Result<T> {

    private T set;

    public Result() {}

    public Result(T set) {
        this.set = set;
    }

}
