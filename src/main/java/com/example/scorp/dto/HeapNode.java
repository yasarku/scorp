package com.example.scorp.dto;

import com.example.scorp.entity.Post;
import lombok.Data;

@Data
public class HeapNode {
    Post post;
    Integer i,j;

    public HeapNode(Post post, Integer i, Integer j) {
        this.post = post;
        this.i = i;
        this.j = j;
    }
}
