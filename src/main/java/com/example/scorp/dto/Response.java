package com.example.scorp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
public class Response {
    
    private int statusCode;
    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;
    private Result result;


    public Response(HttpStatus httpStatus, Result result) {
        this.statusCode = httpStatus.value();
        this.status = httpStatus;
        this.result = result;
        this.timestamp = LocalDateTime.now();
    }
}
